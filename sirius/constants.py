from collections import OrderedDict

from django.utils.translation import gettext_lazy


ROLE_PLAYER = 1
ROLE_COACH = 2

ROLE_CHOICES = OrderedDict(
    [
        (ROLE_PLAYER, gettext_lazy("label_role_player")),
        (ROLE_COACH, gettext_lazy("label_role_coach")),
    ]
)


TYPE_PRACTICE = 1
TYPE_FRIENDLY_MATCH = 2

TYPE_CHOICES = (
    (TYPE_PRACTICE, gettext_lazy("label_practice")),
    (TYPE_FRIENDLY_MATCH, gettext_lazy("label_friendly_match")),
)


TEAM_PRACTICE_COMPETITION = "competition"
TEAM_PRACTICE_LEISURE = "leisure"
TEAM_PRACTICE_FRIENDS = "friends"

TEAM_PRACTICE_CHOICES = (
    (TEAM_PRACTICE_COMPETITION, gettext_lazy("label_team_practice_competition")),
    (TEAM_PRACTICE_LEISURE, gettext_lazy("label_team_practice_leisure")),
    (TEAM_PRACTICE_FRIENDS, gettext_lazy("label_team_practice_friends")),
)

TEAM_AGE_ALL = "aa"

TEAM_AGE_U5 = "U5"
TEAM_AGE_U6 = "U6"
TEAM_AGE_U7 = "U7"
TEAM_AGE_U8 = "U8"
TEAM_AGE_U9 = "U9"
TEAM_AGE_U10 = "U10"
TEAM_AGE_U11 = "U11"
TEAM_AGE_U12 = "U12"
TEAM_AGE_U13 = "U13"
TEAM_AGE_U14 = "U14"
TEAM_AGE_U15 = "U15"
TEAM_AGE_U16 = "U16"
TEAM_AGE_U17 = "U17"
TEAM_AGE_U18 = "U18"
TEAM_AGE_U19 = "U19"
TEAM_AGE_U20 = "U20"
TEAM_AGE_U21 = "U21"

TEAM_AGE_YOUTH = (
    (TEAM_AGE_U5, gettext_lazy("label_team_age_U5")),
    (TEAM_AGE_U6, gettext_lazy("label_team_age_U6")),
    (TEAM_AGE_U7, gettext_lazy("label_team_age_U7")),
    (TEAM_AGE_U8, gettext_lazy("label_team_age_U8")),
    (TEAM_AGE_U9, gettext_lazy("label_team_age_U9")),
    (TEAM_AGE_U10, gettext_lazy("label_team_age_U10")),
    (TEAM_AGE_U11, gettext_lazy("label_team_age_U11")),
    (TEAM_AGE_U12, gettext_lazy("label_team_age_U12")),
    (TEAM_AGE_U13, gettext_lazy("label_team_age_U13")),
    (TEAM_AGE_U14, gettext_lazy("label_team_age_U14")),
    (TEAM_AGE_U15, gettext_lazy("label_team_age_U15")),
    (TEAM_AGE_U16, gettext_lazy("label_team_age_U16")),
    (TEAM_AGE_U17, gettext_lazy("label_team_age_U17")),
    (TEAM_AGE_U18, gettext_lazy("label_team_age_U18")),
    (TEAM_AGE_U19, gettext_lazy("label_team_age_U19")),
    (TEAM_AGE_U20, gettext_lazy("label_team_age_U20")),
    (TEAM_AGE_U21, gettext_lazy("label_team_age_U21")),
)


TEAM_AGE_CHOICES = (
    (TEAM_AGE_ALL, gettext_lazy("label_team_age_all")),
    (gettext_lazy("label_team_age_youth"), TEAM_AGE_YOUTH),
)
