from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from core.models import Event
from api.serializers import EventSerializer


class EventViewSet(viewsets.ModelViewSet):

    queryset = Event.objects.all().order_by("created_at")
    serializer_class = EventSerializer
    permission_classes = [IsAuthenticated]
    http_method_names = ["get"]
