import logging

from django.conf import settings

from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
from rest_framework.response import Response

from api.serializers import ContactSerializer
from core.actions import get_members
from utils.agiclecrm import AgileCRMClient
from utils.calendar import get_mondays


logger = logging.getLogger(__name__)


class DummyView(APIView):

    authentication_classes = []
    permission_classes = [AllowAny]

    def get(self, request):
        logging.debug("API DummyView.get")

        crm_client = AgileCRMClient("contact@email.com", settings.CRM_API_KEY, "se")

        response = dict()
        response["foo"] = "bar"
        response["user"] = {"id": request.user.id, "email": getattr(request.user, "email", None)}
        response["contact"] = ContactSerializer(
            {
                "id": 42,
                "first_name": "John",
                "last_name": "Doe",
                "email": "john.doe@email.com",
            }
        ).data
        response["mondays"] = get_mondays()
        response["members"] = get_members(team_id=1)
        response["crm_client_name"] = crm_client.get_name()
        return Response(response)
