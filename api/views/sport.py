from rest_framework import viewsets

from core.models import Sport
from api.serializers import SportSerializer


class SportViewSet(viewsets.ModelViewSet):

    queryset = Sport.objects.all().order_by("name")
    serializer_class = SportSerializer
    permission_classes = []
    http_method_names = ["get"]
