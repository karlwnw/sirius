from rest_framework import viewsets

from core.models import Country
from api.serializers import CountrySerializer


class CountryViewSet(viewsets.ModelViewSet):

    queryset = Country.objects.all().order_by("name")
    serializer_class = CountrySerializer
    permission_classes = []
    http_method_names = ["get"]
