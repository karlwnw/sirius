from rest_framework import viewsets

from core.models import Team
from api.serializers import TeamSerializer


class TeamViewSet(viewsets.ModelViewSet):

    queryset = Team.objects.all().order_by("created_at")
    serializer_class = TeamSerializer
    permission_classes = []
    http_method_names = ["get"]
