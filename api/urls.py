from django.urls import path, include

from rest_framework import routers

from api.views.sport import SportViewSet
from api.views.country import CountryViewSet
from api.views.event import EventViewSet
from api.views.team import TeamViewSet
from api.views.dummy import DummyView
from api.views.login import LoginView, LogoutView

router = routers.DefaultRouter()
router.register("sports", SportViewSet)
router.register("countries", CountryViewSet)
router.register("events", EventViewSet)
router.register("teams", TeamViewSet)

urlpatterns = [
    path("", include(router.urls)),
    path("login/", LoginView.as_view(), name="user-login"),
    path("logout/", LogoutView.as_view(), name="user-logout"),
    path("dummy/", DummyView.as_view()),
]
