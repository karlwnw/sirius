from django.contrib.auth import authenticate

from rest_framework import serializers

from core.models import Event, Sport, Country, User, Team


class TeamSerializer(serializers.ModelSerializer):
    class Meta:
        model = Team
        fields = ["id", "name", "age_range", "country", "sport_id", "created_at"]


class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = ["name", "team_id", "start_at", "type", "type_display", "created_at"]

    type_display = serializers.SerializerMethodField()

    def get_type_display(self, obj):
        return obj.get_type_display()


class SportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sport
        fields = ["name", "slug_name"]


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = ["name", "iso2"]


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            "id",
            "email",
            "first_name",
            "last_name",
            "created_at",
        ]
        read_only_fields = ("id", "created_at")


class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField()

    def validate(self, attrs):
        user = authenticate(username=attrs["email"], password=attrs["password"])

        if not user:
            raise serializers.ValidationError("Incorrect email or password.")

        if not user.is_active:
            raise serializers.ValidationError("User is disabled.")

        return {"user": user}


class ContactSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    first_name = serializers.CharField(required=False, max_length=30)
    last_name = serializers.CharField(required=False, max_length=30)
    email = serializers.CharField(required=False, max_length=100, read_only=True)
    avatar = serializers.CharField(required=False, read_only=True, max_length=255)
    activated_at = serializers.DateTimeField(read_only=True)

    def to_representation(self, instance):
        output = super().to_representation(instance)
        keep_fields = ("id", "first_name", "last_name", "email")
        return {key: value for key, value in output.items() if key in keep_fields and value}
