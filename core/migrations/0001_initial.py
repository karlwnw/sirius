from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ("auth", "0012_alter_user_first_name_max_length"),
        ("contenttypes", "0002_remove_content_type_name"),
    ]

    operations = [
        migrations.CreateModel(
            name="User",
            fields=[
                ("id", models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("password", models.CharField(max_length=128, verbose_name="password")),
                ("last_login", models.DateTimeField(blank=True, null=True, verbose_name="last login")),
                (
                    "is_superuser",
                    models.BooleanField(
                        default=False,
                        help_text="Designates that this user has all permissions without explicitly assigning them.",
                        verbose_name="superuser status",
                    ),
                ),
                ("email", models.EmailField(max_length=254, unique=True, verbose_name="email")),
                ("first_name", models.CharField(blank=True, max_length=30, verbose_name="first name")),
                ("last_name", models.CharField(blank=True, max_length=30, verbose_name="last name")),
                ("created_at", models.DateTimeField(auto_now_add=True)),
                (
                    "groups",
                    models.ManyToManyField(
                        blank=True,
                        help_text="The groups this user belongs to. A user will get all permissions granted to each of their groups.",
                        related_name="user_set",
                        related_query_name="user",
                        to="auth.Group",
                        verbose_name="groups",
                    ),
                ),
                (
                    "user_permissions",
                    models.ManyToManyField(
                        blank=True,
                        help_text="Specific permissions for this user.",
                        related_name="user_set",
                        related_query_name="user",
                        to="auth.Permission",
                        verbose_name="user permissions",
                    ),
                ),
            ],
            options={
                "db_table": "user",
            },
        ),
        migrations.CreateModel(
            name="Country",
            fields=[
                ("iso2", models.CharField(max_length=2, primary_key=True, serialize=False)),
                ("iso3", models.CharField(max_length=3)),
                ("iso_numeric", models.CharField(max_length=3)),
                ("fips", models.CharField(max_length=2)),
                ("name", models.CharField(max_length=255)),
                (
                    "continent",
                    models.CharField(
                        choices=[
                            ("EU", "Europe"),
                            ("AS", "Asia"),
                            ("AN", "Antarctica"),
                            ("AF", "Africa"),
                            ("OC", "Australia (Oceania)"),
                            ("SA", "South America"),
                            ("NA", "North America"),
                        ],
                        max_length=2,
                    ),
                ),
            ],
            options={
                "db_table": "country",
            },
        ),
        migrations.CreateModel(
            name="Sport",
            fields=[
                ("id", models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("name", models.CharField(db_index=True, max_length=50)),
                ("slug_name", models.SlugField(max_length=64)),
            ],
            options={
                "db_table": "sport",
            },
        ),
        migrations.CreateModel(
            name="Team",
            fields=[
                ("id", models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("name", models.CharField(db_index=True, max_length=250)),
                ("slug_name", models.SlugField(max_length=250)),
                ("presentation", models.TextField(blank=True, null=True)),
                (
                    "age_range",
                    models.CharField(
                        blank=True,
                        choices=[
                            ("aa", "label_team_age_all"),
                            (
                                "label_team_age_youth",
                                (
                                    ("U5", "label_team_age_U5"),
                                    ("U6", "label_team_age_U6"),
                                    ("U7", "label_team_age_U7"),
                                    ("U8", "label_team_age_U8"),
                                    ("U9", "label_team_age_U9"),
                                    ("U10", "label_team_age_U10"),
                                    ("U11", "label_team_age_U11"),
                                    ("U12", "label_team_age_U12"),
                                    ("U13", "label_team_age_U13"),
                                    ("U14", "label_team_age_U14"),
                                    ("U15", "label_team_age_U15"),
                                    ("U16", "label_team_age_U16"),
                                    ("U17", "label_team_age_U17"),
                                    ("U18", "label_team_age_U18"),
                                    ("U19", "label_team_age_U19"),
                                    ("U20", "label_team_age_U20"),
                                    ("U21", "label_team_age_U21"),
                                ),
                            ),
                        ],
                        db_index=True,
                        max_length=3,
                        null=True,
                    ),
                ),
                (
                    "practice_level",
                    models.CharField(
                        blank=True,
                        choices=[
                            ("competition", "label_team_practice_competition"),
                            ("leisure", "label_team_practice_leisure"),
                            ("friends", "label_team_practice_friends"),
                        ],
                        db_index=True,
                        max_length=15,
                        null=True,
                    ),
                ),
                ("created_at", models.DateTimeField(auto_now_add=True)),
                ("updated_at", models.DateTimeField(auto_now=True)),
                ("deleted_at", models.DateTimeField(blank=True, null=True)),
                (
                    "country",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        related_name="teams",
                        to="core.country",
                    ),
                ),
            ],
            options={
                "db_table": "team",
            },
        ),
        migrations.CreateModel(
            name="TeamMember",
            fields=[
                ("id", models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                (
                    "role",
                    models.SmallIntegerField(
                        choices=[(1, "label_role_player"), (2, "label_role_coach")], db_index=True
                    ),
                ),
                ("licence_number", models.CharField(blank=True, max_length=255, null=True)),
                ("created_at", models.DateTimeField(auto_now_add=True)),
                ("updated_at", models.DateTimeField(auto_now=True)),
                (
                    "team",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, related_name="team_members", to="core.team"
                    ),
                ),
                (
                    "user",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="team_members",
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
            options={
                "db_table": "team_member",
                "unique_together": {("team", "user")},
            },
        ),
        migrations.AddField(
            model_name="team",
            name="members",
            field=models.ManyToManyField(related_name="teams", through="core.TeamMember", to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name="team",
            name="sport",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE, related_name="teams", to="core.sport"
            ),
        ),
        migrations.CreateModel(
            name="Tag",
            fields=[
                ("id", models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("slug_name", models.SlugField(max_length=250)),
                ("object_id", models.PositiveIntegerField(blank=True, db_index=True, null=True)),
                ("created_at", models.DateTimeField(auto_now_add=True)),
                (
                    "object_type",
                    models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, to="contenttypes.contenttype"),
                ),
            ],
            options={
                "db_table": "tag",
            },
        ),
        migrations.CreateModel(
            name="Event",
            fields=[
                ("id", models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                (
                    "type",
                    models.SmallIntegerField(
                        choices=[(1, "label_practice"), (2, "label_friendly_match")], db_index=True
                    ),
                ),
                ("start_at", models.DateTimeField(db_index=True)),
                ("end_at", models.DateTimeField(db_index=True, null=True)),
                ("name", models.CharField(blank=True, max_length=64, null=True)),
                ("created_at", models.DateTimeField(auto_now_add=True)),
                ("updated_at", models.DateTimeField(auto_now=True)),
                (
                    "sport",
                    models.ForeignKey(
                        null=True, on_delete=django.db.models.deletion.CASCADE, related_name="events", to="core.sport"
                    ),
                ),
                (
                    "team",
                    models.ForeignKey(
                        null=True, on_delete=django.db.models.deletion.CASCADE, related_name="events", to="core.team"
                    ),
                ),
            ],
            options={
                "db_table": "event",
            },
        ),
    ]
