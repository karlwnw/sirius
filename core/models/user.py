from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models


class UserManager(BaseUserManager):
    def create_user(self, email, password):
        if not email:
            raise ValueError("Users must have an email address")

        user = self.model(email=self.normalize_email(email))
        user.set_password(password)
        user.save()
        return user


class User(AbstractBaseUser, PermissionsMixin):
    class Meta:
        app_label = "core"
        db_table = "user"

    email = models.EmailField("email", max_length=254, unique=True, null=False)

    first_name = models.CharField("first name", max_length=30, blank=True)

    last_name = models.CharField("last name", max_length=30, blank=True)

    created_at = models.DateTimeField(auto_now_add=True, null=False, blank=False)

    objects = UserManager()

    is_staff = False

    USERNAME_FIELD = "email"

    def __str__(self):
        return self.email
