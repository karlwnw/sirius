from django.db import models

CONTINENTS = (
    ("EU", "Europe"),
    ("AS", "Asia"),
    ("AN", "Antarctica"),
    ("AF", "Africa"),
    ("OC", "Australia (Oceania)"),
    ("SA", "South America"),
    ("NA", "North America"),
)


class Country(models.Model):
    class Meta:
        app_label = "core"
        db_table = "country"

    iso2 = models.CharField(max_length=2, primary_key=True)  # ISO-3166 alpha2
    iso3 = models.CharField(max_length=3)  # ISO-3166 alpha3
    iso_numeric = models.CharField(max_length=3)  # ISO-3166 numeric
    fips = models.CharField(max_length=2)  # fips
    name = models.CharField(max_length=255)
    continent = models.CharField(max_length=2, choices=CONTINENTS)

    def __str__(self):
        return "{} ({})".format(self.name, self.iso2)
