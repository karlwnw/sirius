from django.db import models

from sirius.constants import TYPE_CHOICES


class Event(models.Model):
    class Meta:
        app_label = "core"
        db_table = "event"

    type = models.SmallIntegerField(choices=TYPE_CHOICES, db_index=True)

    start_at = models.DateTimeField(db_index=True)

    end_at = models.DateTimeField(null=True, db_index=True)

    team = models.ForeignKey("core.Team", on_delete=models.CASCADE, null=True, related_name="events")

    sport = models.ForeignKey("core.Sport", on_delete=models.CASCADE, null=True, related_name="events")

    name = models.CharField(max_length=64, null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True, null=False, blank=False)
    updated_at = models.DateTimeField(auto_now=True, null=False, blank=False)
