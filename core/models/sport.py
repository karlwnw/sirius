from django.db import models


class Sport(models.Model):
    class Meta:
        app_label = "core"
        db_table = "sport"

    name = models.CharField(max_length=50, db_index=True)
    slug_name = models.SlugField(max_length=64)

    def __str__(self):
        return self.name
