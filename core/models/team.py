from django.db import models
from django.utils.text import slugify

from sirius.constants import TEAM_PRACTICE_CHOICES, TEAM_AGE_CHOICES, ROLE_CHOICES


class Team(models.Model):
    class Meta:
        app_label = "core"
        db_table = "team"

    name = models.CharField(max_length=250, db_index=True)

    slug_name = models.SlugField(max_length=250, db_index=True)

    sport = models.ForeignKey("core.Sport", on_delete=models.CASCADE, related_name="teams")

    presentation = models.TextField(null=True, blank=True)

    age_range = models.CharField(max_length=3, choices=TEAM_AGE_CHOICES, blank=True, null=True, db_index=True)

    practice_level = models.CharField(
        max_length=15,
        choices=TEAM_PRACTICE_CHOICES,
        blank=True,
        null=True,
        db_index=True,
    )

    country = models.ForeignKey(
        "Country",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="teams",
    )

    members = models.ManyToManyField("core.User", through="TeamMember", related_name="teams")

    created_at = models.DateTimeField(auto_now_add=True, null=False, blank=False)
    updated_at = models.DateTimeField(auto_now=True, null=False, blank=False)
    deleted_at = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.slug_name:
            self.slug_name = slugify(self.name)
        super().save(*args, **kwargs)


class TeamMember(models.Model):
    class Meta:
        app_label = "core"
        db_table = "team_member"
        unique_together = ("team", "user")

    team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name="team_members")

    user = models.ForeignKey("core.User", on_delete=models.CASCADE, related_name="team_members")

    role = models.SmallIntegerField(choices=list(ROLE_CHOICES.items()), db_index=True)

    licence_number = models.CharField(blank=True, null=True, max_length=255)

    created_at = models.DateTimeField(auto_now_add=True, null=False, blank=False)
    updated_at = models.DateTimeField(auto_now=True, null=False, blank=False)
