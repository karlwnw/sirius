from django.contrib.contenttypes.models import ContentType
from django.db import models


class Tag(models.Model):
    class Meta:
        app_label = "core"
        db_table = "tag"

    slug_name = models.SlugField(max_length=250, db_index=True)

    object_type = models.ForeignKey(
        ContentType,
        on_delete=models.RESTRICT,
    )
    object_id = models.PositiveIntegerField(db_index=True, null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True, null=False, blank=False)
