from django.contrib.contenttypes.models import ContentType

from core.models import TeamMember, Team, Tag
from utils.external import get_random_users


def create_team(name: str, sport_id: int, owner_id: int):
    Team.objects.create(name=name, sport_id=sport_id, owner_id=owner_id)


def add_user_to_team(team_id: int, user_id: int):
    TeamMember.objects.create(team_id=team_id, user_id=user_id)


def add_team_tags(team_id: int, tags: list):
    team_content_type = ContentType.objects.get_for_model(Team)
    for tag in tags:
        Tag.objects.create(object_id=team_id, object_type_id=team_content_type, slug_name=tag)


def get_members(team_id: int) -> list:
    return get_random_users()
