# Sirius

## Launch

```
./manage.py migrate
./manage.py loaddata core/fixtures/*.json
./manage.py runserver
```

Copyright (C) 2021, SportEasy. All rights reserved.
