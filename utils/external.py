import requests


def get_random_users() -> list:
    response = requests.get("https://randomuser.me/api/")
    assert response.status_code == 200
    return response.json()["results"]
