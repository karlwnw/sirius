import logging

# Documentation: https://github.com/agilecrm/rest-api
from .http import GET, POST, Request

AGILE_API_HOST = "%s.agilecrm.com"

# These two lines enable debugging at httplib level (requests->urllib3->http.client)
# You will see the REQUEST, including HEADERS and DATA, and RESPONSE with HEADERS but without DATA.
# The only thing missing will be the response.body which is not logged.
try:
    import http.client as http_client
except ImportError:
    # Python 2
    import http.client as http_client


class AgileCRMClient:
    def __init__(self, api_email, api_key, host, backend=None, timeout=5):
        if backend is None:
            from .requests import RequestsBackend

            backend = RequestsBackend

        self._api_email = api_email
        self._api_key = api_key
        self._timeout = timeout
        self._host = AGILE_API_HOST % host

        self.http = backend(self, auth=(self.api_email, self.api_key))

    @property
    def scheme(self):
        return "https"

    @property
    def host(self):
        return self._host

    @property
    def api_email(self):
        return self._api_email

    @property
    def api_key(self):
        return self._api_key

    @property
    def timeout(self):
        return self._timeout

    def search_contact(self, email):
        request = Request(self, POST, "api/contacts/search/email", {"email_ids": [email]})
        return self.http.send_request(request)

    def get_all_contacts(self, cursor=None):
        query_params = {"page_size": "20"}
        if cursor is not None:
            query_params.update({"cursor": cursor})
        request = Request(self, GET, "api/contacts", query_params)
        return self.http.send_request(request)

    def get_contact(self, contact_id):
        path = "api/contacts/{contact_id}".format(contact_id=contact_id)
        request = Request(self, GET, path)
        return self.http.send_request(request)

    def get_all_tasks(self):
        request = Request(self, GET, "api/tasks/all")
        return self.http.send_request(request)

    def get_pending_tasks(self):
        request = Request(self, GET, "api/tasks")
        return self.http.send_request(request)

    def create_company(self):
        url = "api/contacts"

    def create_contact(self):
        url = "api/contacts"

    def get_name(self):
        return self.__class__.__name__
