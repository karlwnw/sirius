class AgileCRMError(Exception):
    pass


class AgileCRMBadRequest(AgileCRMError):
    pass


class AgileCRMBadAuth(AgileCRMError):
    pass


class AgileCRMForbidden(AgileCRMError):
    pass


class AgileCRMLimitExceeded(AgileCRMError):
    pass


class AgileCRMBadStatus(AgileCRMError):
    pass
