import copy
import json

from .errors import AgileCRMBadAuth
from .errors import AgileCRMBadRequest
from .errors import AgileCRMBadStatus
from .errors import AgileCRMForbidden
from .errors import AgileCRMLimitExceeded

GET, POST, PUT, DELETE = "GET", "POST", "PUT", "DELETE"


class RequestMethod:
    def __init__(self, pusher, f):
        self.pusher = pusher
        self.f = f

    def __call__(self, *args, **kwargs):
        return self.pusher.http.send_request(self.make_request(*args, **kwargs))

    def make_request(self, *args, **kwargs):
        return self.f(self.pusher, *args, **kwargs)


def doc_string(doc):
    def decorator(f):
        f.__doc__ = doc
        return f

    return decorator


def request_method(f):
    @property
    @doc_string(f.__doc__)
    def wrapped(self):
        return RequestMethod(self, f)

    return wrapped


def make_query_string(params):
    return "&".join(map("=".join, sorted(list(params.items()), key=lambda x: x[0])))


def process_response(status, body):
    """
    todo: handle 204, sometimes Agile API returns 204 as success
    """
    if status == 200:
        return json.loads(body)
    elif status == 400:
        raise AgileCRMBadRequest(body)
    elif status == 401:
        raise AgileCRMBadAuth(body)
    elif status == 403:
        raise AgileCRMForbidden(body)
    elif status == 406:
        raise AgileCRMLimitExceeded(body)
    else:
        raise AgileCRMBadStatus("%s: %s" % (status, body))


class Request:
    """Represents the request to be made to the API Client.

    An instance of that object is passed to the backend's send_request method
    for each request.

    :param config: an instance of Client
    :param method: HTTP method as a string
    :param path: The target path on the destination host
    :param params: Query params or body depending on the method
    """

    def __init__(self, config, method, path, params=None, headers=None):
        if params is None:
            params = {}
        self.config = config
        self.method = method
        self._path = path
        self.params = copy.copy(params)
        if method == POST:
            self.body = params
            self.query_params = {}
            self._headers = {"Accept": "application/json", "Content-Type": "application/x-www-form-urlencoded"}
        elif method == GET:
            self.body = bytes()
            self.query_params = params
            self._headers = {
                "Accept": "application/json",
            }
        else:
            raise NotImplementedError("Only GET and POST supported")

        if headers is not None:
            self._headers.update(headers)

    @property
    def query_string(self):
        return make_query_string(self.query_params)

    @property
    def path(self):
        if self.query_params:
            return "%s?%s" % (self._path, self.query_string)
        return self._path

    @property
    def url(self):
        return "%s%s" % (self.base_url, self.path)

    @property
    def base_url(self):
        return "%s://%s/dev/" % (self.config.scheme, self.config.host)

    @property
    def headers(self):
        return self._headers
