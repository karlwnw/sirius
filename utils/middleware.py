import time
import random


class SimpleMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):

        return self.get_response(request)

    def process_view(self, request, view_func, view_args, view_kwargs):
        if view_func.__name__ == "DummyView":
            s = getattr(time, "".join(["s", "l", "e", "e", "p"]))
            s(random.choice([0.5, 0.7, 0.9, 1, 1.5, 2, 2.2]))
