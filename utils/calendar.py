from django.utils import timezone

from datetime import datetime, timedelta


def datetime_perdelta(start, end, delta):
    curr = start
    while curr < end:
        yield curr
        curr += delta


def get_mondays():
    """
    yield all mondays (as ISO string) in the current year
    """
    start = datetime(timezone.now().year, 1, 1)
    end = datetime(timezone.now().year + 1, 1, 1)

    for d in datetime_perdelta(start, end, timedelta(days=1)):
        if d.weekday() == 0:
            yield d.date().isoformat()
